package com.example.baggagecalculator.calculator;

public class Passenger {
    private int SeatClass,AgeClass,PassengerClass;//座舱类型,客票年龄类型,旅客类型
    private boolean DorI;//true表示国内航班，false表示国际航班
    private double price;//国内航班票价（国内）
    private int area;//国际航班区域（国际）
    private boolean areaB;//国际航班经济舱旅客是否可免费托运两件行李（国际）

    Passenger(int SeatClass,int AgeClass,int PassengerClass,boolean DorI){
        this.SeatClass=SeatClass;
        this.AgeClass=AgeClass;
        this.PassengerClass=PassengerClass;
        this.DorI=DorI;
    }

    public void setAreaB(boolean areaB){
        this.areaB=areaB;
    }

    public void setArea(int area){
        this.area=area;
    }

    public void setPrice(int price){
        this.price=price;
    }

    public int getSeatClass(){
        return SeatClass;
    }

    public int getAgeClass(){
        return AgeClass;
    }

    public int getPassengerClass(){
        return PassengerClass;
    }

    public boolean getDorI(){
        return DorI;
    }

    public double getPrice(){
        return price;
    }

    public int getArea(){
        return area;
    }

    public boolean getAreaB(){
        return areaB;
    }
}
