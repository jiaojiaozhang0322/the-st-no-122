package com.example.baggagecalculator.calculator;
import java.util.Vector;

public class BaggageCalculator {
    /**
     * 判定某乘客的一件行李从属的类别
     * @param P 乘客信息
     * @param B 行李信息
     * @return 该行李对应该乘客所属的最适应类别
     * 0表示可随身携带，1表示满足机票托运限制，2表示满足额外托运限制，3表示超限但可托运，4表示超限且不可托运/携带
     */
    private static int baggageClass(Passenger P, Baggage B){
        boolean DorI=P.getDorI();
        int SeatClass=P.getSeatClass();
        int[] MWN=GetWeightAndNum.MaxWeightAndNum(P);
        int extraW=GetWeightAndNum.ExtraWeight(P);
        int[] CarryLWH={55,40,20};//随身携带行李长宽高最大值
        int[] CheckLWH_D={100,60,40};//托运行李长宽高最大值（国内）
        int Bclass=4;//行李类别

        //是否可随身携带
        if(B.getLength()<=CarryLWH[0] && B.getWidth()<=CarryLWH[1] && B.getHeight()<=CarryLWH[2]){//长宽高满足
            if(SeatClass<3){//经济舱
                if(B.getWeight()<=5) return 0;
            }else{//商务舱，头等舱
                if(B.getWeight()<=8) return 0;
            }
        }

        //可托运
        if(DorI){//国内航班
            if(B.getLength()>CheckLWH_D[0] ||B.getWidth()>CheckLWH_D[1] ||B.getHeight()>CheckLWH_D[2] ||B.getSize()<60){
                return Bclass;//尺寸超限，不可托运
            }
            if(MWN[0]==7){//商务舱，限制特殊
                if(B.getWeight() <= extraW){//商务舱机票自带行李重量额度一定大于额外额度
                    Bclass=2;
                } else if (B.getWeight() <= 32) {
                    Bclass=1;
                }
            }else{
                if(extraW>MWN[1]){//额外行李重量额度大于机票自带
                    if (B.getWeight() <= MWN[1]) {
                        Bclass=1;
                    } else if (B.getWeight() <= extraW) {
                        Bclass=2;
                    } else if (B.getWeight() <= 32) {
                        Bclass=3;
                    }
                } else if (extraW<MWN[1]) {//额外行李重量额度小于机票自带
                    if (B.getWeight() <= extraW) {
                        Bclass=2;
                    } else if (B.getWeight() <= MWN[1]) {
                        Bclass=1;
                    } else if (B.getWeight() <= 32) {
                        Bclass=3;
                    }
                } else {//额外行李重量额度等于机票自带
                    if (B.getWeight() <= MWN[1]) {
                        Bclass=1;
                    } else if (B.getWeight() <= 32) {
                        Bclass=3;
                    }
                }
            }
        }else{//国际航班
            //国际航班无额外行李托运额度，故只能返回1或3
            if(B.getSize()<60){
                //不可托运
            } else if (B.getSize()<=158){
                if(B.getWeight()<=MWN[1]){
                    Bclass=1;
                } else if (B.getWeight()<=32) {
                    Bclass=3;
                }
            } else if (B.getSize()<=203) {
                if(B.getWeight()<=32) Bclass=3;
            }
        }

        return Bclass;
    }


    /**
     *计算得到使国内航班超重行李托运费用最低的托运方案及相应费用
     * @param P 乘客信息
     * @param Bs 乘客的行李信息集合
     * @param carryB 随身携带行李集合
     * @param checkB 机票托运行李集合
     * @param extraB 额外托运行李集合
     * @param overB 超重量/数量行李集合
     * @param unableB 无法托运/随身携带行李集合
     * @return 超重行李托运费用
     */
    public static double baggageCalculatorD(Passenger P, Baggage[] Bs, Vector<Integer> carryB, Vector<Integer> checkB,
                                         Vector<Integer> extraB, Vector<Integer> overB, Vector<Integer> unableB){
        int[] MWN=GetWeightAndNum.MaxWeightAndNum(P);//机票托运行李数量和重量限制
        int extraW=GetWeightAndNum.ExtraWeight(P);//额外托运行李重量限制
        int carryN=GetWeightAndNum.CarryNum(P);//可随身携带行李数量
        int[] BsClass=new int[Bs.length];//行李类别集合
        int overWeight=0;//超重总重量
        double overCost=0;//超重行李托运费用

        //首先，根据各行李类别对其进行划分
        classifyBS(P, Bs, BsClass, carryB, checkB, extraB, overB, unableB);

        //由于国内航班的行李超重收费只按重量计算，所以我们需尽量使可免费托运/携带的行李重量最大
        /**一，处理随身携带行李
         * 在满足旅客对随身携带行李的硬性要求的同时，使随身携带的行李重量最大
         */
        if(carryB.size()>carryN){//满足可随身携带条件的行李数超过可随身携带的最大件数
            Vector<Integer> cb=(Vector<Integer>) carryB.clone();
            carryB.clear();
            carryB_correction(carryB, cb, Bs, carryN);

            //把剩余行李放入下一等级
            if(MWN[1]<extraW || extraW==0){//下一等级为机票托运行李或无额外托运额度
                for(Integer i:cb) {
                    if (carryB.indexOf(i) == -1) {
                        System.out.println(Bs[i].getWeight());
                        checkB.add(i);
                    }
                }
            }else{
                for(Integer i:cb) {//下一等级为额外托运行李
                    if (carryB.indexOf(i) == -1) extraB.add(i);
                }
            }
        }

        /**二，处理正常托运行李
         * 使可正常免费托运的行李（包括机票自带和额外）的重量最大
         */
        if(MWN[1]<extraW){//下一等级为机票托运行李
            if(checkB.size()>MWN[0]){//满足机票托运条件的行李数超过机票托运最大件数
                Vector<Integer> cb=(Vector<Integer>) checkB.clone();
                checkB.clear();
                checkB_correction(checkB, cb, Bs,MWN);

                //把剩余行李放入下一等级，即额外托运行李
                for(Integer i:cb) {
                    if (checkB.indexOf(i) == -1) extraB.add(i);
                }
            }

            //对下一等级，即额外托运行李进行处理
            if(extraB.size()>1){//满足额外托运条件的行李数超过1（额外行李件数都是1）
                Vector<Integer> eb=(Vector<Integer>) extraB.clone();
                extraB.clear();
                extraB_correction(extraB, eb, Bs);

                //把剩余行李放入下一等级，即超重量/数量行李
                for(Integer i:eb) {
                    if (extraB.indexOf(i) == -1) overB.add(i);
                }
            }

        }else{//下一等级为额外托运行李
            if(extraB.size()>1){//满足额外托运条件的行李数超过1（额外行李件数都是1）
                Vector<Integer> eb=(Vector<Integer>) extraB.clone();
                extraB.clear();
                extraB_correction(extraB, eb, Bs);

                //把剩余行李放入下一等级，即机票托运行李
                for(Integer i:eb) {
                    if (extraB.indexOf(i) == -1) checkB.add(i);
                }
            }

            //对下一等级，即机票托运行李进行处理
            if(checkB.size()>MWN[0]){//满足机票托运条件的行李数超过机票托运最大件数
                Vector<Integer> cb=(Vector<Integer>) checkB.clone();
                checkB.clear();
                checkB_correction(checkB, cb, Bs,MWN);

                //把剩余行李放入下一等级，即超重量/数量行李
                for(Integer i:cb) {
                    if (checkB.indexOf(i) == -1) overB.add(i);
                }
            }

        }

        /**
         * 三，处理超重量/数量行李
         * 以超重重量最小为原则，处理超重量/数量的行李
         */
        //国内航班头等舱的特殊情况
        if(MWN[0]==7){
            //对机票托运行李再处理，保证使用机票托运的行李满足限制且重量最大
            if(checkB.size()>1){
                int mx=0;
                int mi=-1,mj=-1;
                //找出单件重量或两件重量和最大的行李
                for(int i=0; i<checkB.size(); i++){
                    int a=Bs[checkB.elementAt(i)].getWeight();
                    if(a<=32 && a>mx){
                        mx=a;
                        mi=checkB.elementAt(i);
                        mj=-1;
                    }
                    for(int j=0; j<i; j++){
                        int b=Bs[checkB.elementAt(j)].getWeight();
                        if(a+b<=40 && a+b>mx){
                            mx=a+b;
                            mi=checkB.elementAt(i);
                            mj=checkB.elementAt(j);
                        }
                    }
                }

                Vector<Integer> cb=(Vector<Integer>) checkB.clone();
                checkB.clear();
                if(mi!=-1){
                    checkB.add(mi);
                    if(mj!=-1) checkB.add(mj);
                }

                //把剩余行李放入下一等级，即超重量/数量行李
                for(Integer i:cb) {
                    if (checkB.indexOf(i) == -1) overB.add(i);
                }

                //国内航班商务舱行李的初始分类无3，即超限但可托运行李，故此时的overB中全为后期对checkB和extraB处理后再放入的行李序号
                for(Integer i:overB) overWeight+=Bs[i].getWeight();
                if(overWeight>0) overWeight-=((1-extraB.size())*extraW);
                overCost=overWeight*P.getPrice()*0.015;
            }

            return overCost;
        }


        //非国内航班商务舱的其它情况
        if(MWN[1]<extraW){//下一等级为机票托运行李
            int c=MWN[0]-checkB.size();
            int e=1-extraB.size();
            for(Integer i:overB) overWeight+=Bs[i].getWeight();
            if(overB.size()>=c+e){
                overWeight-=(c*MWN[1]+e*extraW);
            } else if (overB.size()>=e) {
                overWeight-=(e*extraW+(overB.size()-e)*MWN[1]);
            } else{
                overWeight-=(overB.size()*extraW);
            }

        }else{//下一等级为额外托运行李
            int c=MWN[0]-checkB.size();
            int e=1-extraB.size();
            for(Integer i:overB) overWeight+=Bs[i].getWeight();
            if(overB.size()>=c+e){
                overWeight-=(c*MWN[1]+e*extraW);
            } else if (overB.size()>=c) {
                overWeight-=(c*MWN[1]+(overB.size()-c)*extraW);
            }else{
                overWeight-=(overB.size()*MWN[1]);
            }
        }

        overCost=overWeight*P.getPrice()*0.015;
        return overCost;
    }


    /**
     计算得到使国际航班超重行李托运费用最低的托运方案及相应费用
     * @param P 乘客信息
     * @param Bs 乘客的行李信息集合
     * @param carryB 随身携带行李集合
     * @param checkB 机票托运行李集合
     * @param overB 超重量/数量行李集合
     * @param unableB 无法托运/随身携带行李集合
     * @return 超重行李托运费用数组
     */
    public static int[] baggageCalculatorI(Passenger P, Baggage[] Bs, Vector<Integer> carryB, Vector<Integer> checkB,
                                            Vector<Integer> overB, Vector<Integer> unableB){
        int[] MWN=GetWeightAndNum.MaxWeightAndNum(P);//机票托运行李数量和重量限制
        int carryN=GetWeightAndNum.CarryNum(P);//可随身携带行李数量
        int[] BsClass=new int[Bs.length];//行李类别集合
        Vector<Integer> extraB=new Vector<>();//无此项

        int currency=2;
        if(P.getArea()==2||P.getArea()==3){//含欧元区域
            currency=3;
        }
        int[] overCost=new int[currency];//超重行李托运费用
        for(int i=0; i<currency; i++) overCost[i]=0;//初始化

        //首先，根据各行李类别对其进行划分
        classifyBS(P, Bs, BsClass, carryB, checkB, extraB, overB, unableB);

        /**一，处理随身携带行李
         * 在满足旅客对随身携带行李的硬性要求的同时，使随身携带的行李重量最大
         */
        if(carryB.size()>carryN){//满足可随身携带条件的行李数超过可随身携带的最大件数
            //for(Integer i:carryB) System.out.println(Bs[i].getWeight());
            Vector<Integer> cb=(Vector<Integer>) carryB.clone();
            carryB.clear();
            carryB_correction(carryB, cb, Bs, carryN);

            //System.out.println('a');
            //for(Integer i:carryB) System.out.println(Bs[i].getWeight());
            //把剩余行李放入下一等级
            for(Integer i:cb) {
                if (carryB.indexOf(i) == -1) checkB.add(i);
            }
        }

        /**二，处理正常托运行李
         * 使可正常免费托运的行李的重量最大
         */
        if(checkB.size()>MWN[0]){//满足机票托运条件的行李数超过机票托运最大件数
            Vector<Integer> cb=(Vector<Integer>) checkB.clone();
            checkB.clear();
            checkB_correction(checkB, cb, Bs,MWN);

            //把剩余行李放入下一等级，即超重量/数量行李
            for(Integer i:cb) {
                if (checkB.indexOf(i) == -1) overB.add(i);
            }
        }

        /**
         * 三，处理超重量/数量行李
         */
        //超重量/尺寸处理
        for(Integer i:overB){
            int[] ows=OverChargeI.OverWSChargeI(P,Bs[i]);
            for(int k=0; k<currency; k++) overCost[k]+=ows[k];
        }
        //超数量处理
        int overNum=overB.size()-(MWN[0]-checkB.size());
        int[] on=OverChargeI.OverNumChargeI(P,overNum);
        for(int k=0; k<currency; k++) overCost[k]+=on[k];

        return overCost;

    }


    /**
     * 根据各行李类别对其进行划分
     * @param P 乘客信息
     * @param Bs 乘客的行李信息集合
     * @param BsClass 乘客的行李类别集合
     * @param carryB 随身携带行李集合
     * @param checkB 机票托运行李集合
     * @param extraB 额外托运行李集合
     * @param overB 超重量/数量行李集合
     * @param unableB 无法托运/随身携带行李集合
     */
    private static void classifyBS(Passenger P, Baggage[] Bs, int[] BsClass, Vector<Integer> carryB, Vector<Integer> checkB,
                                   Vector<Integer> extraB, Vector<Integer> overB, Vector<Integer> unableB){
        for(int i=0; i<Bs.length; i++){
            BsClass[i]=baggageClass(P,Bs[i]);
            if(Bs[i].isCarry() && BsClass[i]!=0){
                unableB.add(i);
            }else{
                switch (BsClass[i]){
                    case 0:
                        carryB.add(i);
                        break;
                    case 1:
                        checkB.add(i);
                        break;
                    case 2:
                        extraB.add(i);
                        break;
                    case 3:
                        overB.add(i);
                        break;
                    case 4:
                        unableB.add(i);
                        break;
                }
            }
        }
    }

    /**
     * 对随身携带行李集合进行修正，使其符合要求且重量最大
     * 但对必须携带的行李数量超限的情况无修正作用
     * @param carryB 随身携带行李集合
     * @param cb 原随身携带行李集合的拷贝
     * @param Bs 乘客的行李信息集合
     * @param carryN 可随身携带行李最大件数
     */
    private static void carryB_correction(Vector<Integer> carryB, Vector<Integer> cb, Baggage[] Bs, int carryN){
        for(Integer i:cb){
            //重量小于2kg的行李无法托运，只能随身携带；已限定随身携带的行李
            if(Bs[i].getWeight()<2 || Bs[i].isCarry()) {
                if(carryB.size()<2) carryB.add(i);
            }
        }

        int ec=carryN-carryB.size();//还能携带的行李数量
        if(ec==1){//还能随身携带一件行李，在剩余符合条件的行李中取重量最大的一件
            int m=-1,mw=0;
            for(Integer i:cb){
                if(carryB.indexOf(i)==-1 && Bs[i].getWeight()>mw){
                    mw=Bs[i].getWeight();
                    m=i;
                }
            }
            if(m!=-1){
                carryB.add(m);
            }
        } else if (ec==2) {//还能随身携带两件行李，在剩余符合条件的行李中取重量最大的两件
            int m=-1,n=-1;
            int mw=0,nw=0;
            for(Integer i:cb){
                if(carryB.indexOf(i)==-1){
                    if(Bs[i].getWeight()>mw){
                        nw=mw;
                        n=m;
                        mw=Bs[i].getWeight();
                        m=i;
                    } else if (Bs[i].getWeight()>nw) {
                        nw=Bs[i].getWeight();
                        n=i;
                    }
                }
            }
            if(m!=-1){
                carryB.add(m);
            }
            if(n!=-1){
                carryB.add(n);
            }
        }

    }

    /**
     * 对机票托运行李集合进行修正，使其符合要求且重量最大
     * @param checkB 机票托运行李集合
     * @param cb 原机票托运行李集合的拷贝
     * @param Bs 乘客的行李信息集合
     * @param MWN 机票托运行李数量和重量限制
     */
    private static void checkB_correction(Vector<Integer> checkB, Vector<Integer> cb, Baggage[] Bs, int[] MWN){
        if(MWN[0]==1){//机票托运额度为一件行李，在符合条件的行李中取重量最大的一件
            int m=-1,mw=0;
            for(Integer i:cb){
                if(Bs[i].getWeight()>mw){
                    mw=Bs[i].getWeight();
                    m=i;
                }
            }
            if(m!=-1){
                checkB.add(m);
            }
        }else{//机票托运额度为两件行李，在符合条件的行李中取重量最大的两件
            int m=-1,n=-1;
            int mw=0,nw=0;
            for(Integer i:cb){
                if(Bs[i].getWeight()>mw){
                    nw=mw;
                    n=m;
                    mw=Bs[i].getWeight();
                    m=i;
                } else if (Bs[i].getWeight()>nw) {
                    nw=Bs[i].getWeight();
                    n=i;
                }
            }
            if(m!=-1){
                checkB.add(m);
            }
            if(n!=-1){
                checkB.add(n);
            }
        }
    }

    /**
     * 对额外托运行李集合进行修正，使其符合要求且重量最大
     * @param extraB 额外托运行李集合
     * @param eb 原额外托运行李集合的拷贝
     * @param Bs 乘客信息集合
     */
    private static void extraB_correction(Vector<Integer> extraB, Vector<Integer> eb, Baggage[] Bs){
        int m=-1,mw=0;
        for(Integer i:eb){
            if(Bs[i].getWeight()>mw){
                mw=Bs[i].getWeight();
                m=i;
            }
        }
        if(m!=-1){
            extraB.add(m);
        }
    }
}
