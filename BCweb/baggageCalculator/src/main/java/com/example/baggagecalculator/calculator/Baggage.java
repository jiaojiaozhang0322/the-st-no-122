package com.example.baggagecalculator.calculator;

public class Baggage {
    private int length,width,height;//行李长宽高
    private int size;//行李长宽高之和
    private int weight;//行李重量
    private boolean isCarry;//是否指定随身携带

    Baggage(int length,int width,int height,int weight,boolean isCarry){
        this.length=length;
        this.width=width;
        this.height=height;
        this.size=length+width+height;
        this.weight=weight;
        this.isCarry=isCarry;
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }

    public int getLength() {
        return length;
    }

    public int getSize() {
        return size;
    }

    public int getWeight() {
        return weight;
    }

    public boolean isCarry() {
        return isCarry;
    }
}
