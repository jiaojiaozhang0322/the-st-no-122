package com.example.baggagecalculator.calculator;

public class GetWeightAndNum {
    /**
     * 获取旅客机票对应的最大托运行李数与行李重量
     * @param P 旅客信息
     * @return 包含两个值的数组，第一个值表示可托运行李件数，第二个值表示每件行李最大重量
     */
    public static int[] MaxWeightAndNum(Passenger P){
        int SeatClass=P.getSeatClass();
        int AgeClass=P.getAgeClass();
        boolean DorI=P.getDorI();
        boolean areaB=P.getAreaB();

        //MWN为包含两个值的数组，第一个值表示可托运行李件数，第二个值表示每件行李最大重量
        int[] MWN=new int[2];
        if(DorI==true){//国内航班
            //国内旅客托运行李无件数限制
            //但出于航空托运空间资源的考虑，我们限制头等舱机票对应行李托运件数不得超过2，总重量不得超过40kg
            //公务舱/经济舱/婴儿客票机票对应行李托运件数不得超过1
            MWN[0]=1;
            if(AgeClass==2){//婴儿客票
                MWN[1]=10;
            }else{//成人或儿童客票
                if(SeatClass==5){//公务舱
                    MWN[1]=30;
                }else if(SeatClass==3||SeatClass==4){//头等舱（大类）
                    MWN[0]=7;//以7表示头等舱托运的特殊限制
                    MWN[1]=40;
                }else{//经济舱（大类）
                    MWN[1]=20;
                }
            }
        }else{//国际航班
            if(SeatClass==3||SeatClass==4||SeatClass==5){//公务舱，头等舱（大类）
                MWN[0]=2;
                MWN[1]=32;
            } else if (SeatClass==1||SeatClass==2) {//悦享经济舱、超级经济舱
                MWN[0]=2;
                MWN[1]=23;
            } else {
                if(areaB==true){//可托运两件行李
                    MWN[0]=2;
                }else{//不可托运两件行李
                    MWN[0]=1;
                }
                MWN[1]=23;
            }
        }

        return MWN;
    }

    /**
     * 获取旅客可额外托运的行李最大重量（件数都为1）
     * @param P 旅客信息
     * @return 可额外托运的行李最大重量
     */
    public static int ExtraWeight(Passenger P){
        boolean DorI=P.getDorI();
        int PassengerClass=P.getPassengerClass();

        //可额外托运的行李最大重量
        int Eweight=0;

        if(DorI==true){//国内航班
            if(PassengerClass==1||PassengerClass==2||PassengerClass==3){
                //星空联盟金卡,凤凰知音金卡、银卡旅客
                Eweight=20;
            } else if (PassengerClass==4||PassengerClass==5) {
                //凤凰知音终身白金卡、白金卡旅客
                Eweight=30;
            }
        }

        return Eweight;
    }

    /**
     * 获取乘客可随身携带的行李数量
     * @param P 乘客信息
     * @return 乘客可随身携带的行李数量
     */
    public static int CarryNum(Passenger P){
        int SeatClass=P.getSeatClass();

        if(SeatClass<3){//经济舱
            return 1;
        }else{//商务舱，头等舱
            return 2;
        }
    }
}
