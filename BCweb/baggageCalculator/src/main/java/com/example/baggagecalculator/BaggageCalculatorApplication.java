package com.example.baggagecalculator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BaggageCalculatorApplication {

	public static void main(String[] args) {
		SpringApplication.run(BaggageCalculatorApplication.class, args);
	}

}
