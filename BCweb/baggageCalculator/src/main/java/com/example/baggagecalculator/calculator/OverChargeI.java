package com.example.baggagecalculator.calculator;

public class OverChargeI {
    /**
     * 计算国际航班旅客一件超重/超尺寸行李的费用
     * @param P 旅客信息
     * @param B 行李信息
     * @return 人民币，美元，（欧元）超重费用组成的数组
     */
    public static int[] OverWSChargeI(Passenger P, Baggage B ){
        int size=B.getSize();
        int weight=B.getWeight();
        int area=P.getArea();
        int[] cost;//人民币，美元，（欧元）超重费用

        if(area==2||area==3){//含欧元区域
            cost=new int[3];
        }else{//不含欧元区域
            cost=new int[2];
        }

        switch (area){
            case 1://区域一
                if(size>=60 && size<=158){//不超尺寸
                    if(weight>23 && weight<=28){//超重量但不超尺寸Ⅰ
                        cost[0]=380;
                        cost[1]=60;
                    } else if (weight>28 && weight<=32) {//超重量但不超尺寸Ⅱ
                        cost[0]=980;
                        cost[1]=150;
                    }
                } else if (size>158 && size<=203) {//超尺寸
                    if(weight>=2 && weight<=23){//不超重量但超尺寸
                        cost[0]=980;
                        cost[1]=150;
                    } else if (weight>23 && weight<=32) {//超重量且超尺寸
                        cost[0]=1400;
                        cost[1]=220;
                    }
                }
                break;
            case 2://区域二
                if(size>=60 && size<=158){//不超尺寸
                    if(weight>23 && weight<=28){//超重量但不超尺寸Ⅰ
                        cost[0]=280;
                        cost[1]=40;
                        cost[2]=35;
                    } else if (weight>28 && weight<=32) {//超重量但不超尺寸Ⅱ
                        cost[0]=690;
                        cost[1]=100;
                        cost[2]=85;
                    }
                } else if (size>158 && size<=203) {//超尺寸
                    if(weight>=2 && weight<=23){//不超重量但超尺寸
                        cost[0]=690;
                        cost[1]=100;
                        cost[2]=85;
                    } else if (weight>23 && weight<=32) {//超重量且超尺寸
                        cost[0]=1100;
                        cost[1]=160;
                        cost[2]=140;
                    }
                }
                break;
            case 3://区域三
                if((size>158 && size<=203)||(weight>23 && weight<=32)){
                    cost[0]=520;
                    cost[1]=75;
                    cost[2]=100;
                }
                break;
            case 4://区域四
                if(size>=60 && size<=158){//不超尺寸
                    if(weight>23 && weight<=28){//超重量但不超尺寸Ⅰ
                        cost[0]=690;
                        cost[1]=100;
                    } else if (weight>28 && weight<=32) {//超重量但不超尺寸Ⅱ
                        cost[0]=1040;
                        cost[1]=150;
                    }
                } else if (size>158 && size<=203) {//超尺寸
                    if(weight>=2 && weight<=23){//不超重量但超尺寸
                        cost[0]=1040;
                        cost[1]=150;
                    } else if (weight>23 && weight<=32) {//超重量且超尺寸
                        cost[0]=2050;
                        cost[1]=300;
                    }
                }
                break;
            case 5://区域五
                if(size>=60 && size<=158){//不超尺寸
                    if(weight>23 && weight<=28){//超重量但不超尺寸Ⅰ
                        cost[0]=210;
                        cost[1]=30;
                    } else if (weight>28 && weight<=32) {//超重量但不超尺寸Ⅱ
                        cost[0]=520;
                        cost[1]=75;
                    }
                } else if (size>158 && size<=203) {//超尺寸
                    if(weight>=2 && weight<=23){//不超重量但超尺寸
                        cost[0]=520;
                        cost[1]=75;
                    } else if (weight>23 && weight<=32) {//超重量且超尺寸
                        cost[0]=830;
                        cost[1]=120;
                    }
                }
                break;
        }

        return cost;
    }


    /**
     * 计算国际航班旅客额外行李费用
     * @param P 旅客信息
     * @param overNum 超出行李件数
     * @return 人民币，美元，（欧元）超重费用组成的数组
     */
    public static int[] OverNumChargeI(Passenger P,int overNum){
        int area=P.getArea();
        int[] cost;//人民币，美元，（欧元）超重费用

        if(area==2||area==3){//含欧元区域
            cost=new int[3];
        }else{//不含欧元区域
            cost=new int[2];
        }

        if(overNum<=0) return cost;

        switch (area){
            case 1://区域一
                if(overNum==1){
                    cost[0]=1400;
                    cost[1]=220;
                } else if (overNum==2) {
                    cost[0]=1400+2000;
                    cost[1]=220+310;
                } else {
                    cost[0]=1400+2000+3000*(overNum-2);
                    cost[1]=220+310+460*(overNum-2);
                }
                break;
            case 2://区域二
                if(overNum<3){
                    cost[0]=1100*overNum;
                    cost[1]=160*overNum;
                    cost[2]=140*overNum;
                } else {
                    cost[0]=2200+1590*(overNum-2);
                    cost[1]=320+230*(overNum-2);
                    cost[2]=280+200*(overNum-2);
                }
                break;
            case 3://区域三
                if(overNum<3){
                    cost[0]=1170*overNum;
                    cost[1]=110*overNum;
                    cost[2]=225*overNum;
                } else {
                    cost[0]=2340+1590*(overNum-2);
                    cost[1]=220+230*(overNum-2);
                    cost[2]=450+300*(overNum-2);
                }
                break;
            case 4://区域四
                if(overNum<3){
                    cost[0]=1380*overNum;
                    cost[1]=200*overNum;
                } else {
                    cost[0]=2760+1590*(overNum-2);
                    cost[1]=400+230*(overNum-2);
                }
                break;
            case 5://区域五
                if(overNum==1){
                    cost[0]=830;
                    cost[1]=120;
                } else if (overNum==2) {
                    cost[0]=830+1100;
                    cost[1]=120+160;
                } else {
                    cost[0]=830+1100+1590*(overNum-2);
                    cost[1]=120+160+230*(overNum-2);
                }
                break;
        }

        return cost;
    }
}
