package com.example.baggagecalculator.calculator;

public class AreaCalculator {
    /**area矩阵存储始发地与目的地之间的航班对应的区域
     * area[i][j]=m中i代表始发地，j代表目的地，m代表航班属于区域m;矩阵为对称矩阵
     * 0：亚洲（除日本，巴基斯坦，新加坡，哈萨克斯坦）
     * 1：日本
     * 2：巴基斯坦
     * 3：新加坡
     * 4：哈萨克斯坦
     * 5：西南太平洋
     * 6：中东
     * 7：欧洲
     * 8：非洲
     * 9：美洲（除美国，加拿大）
     * 10：加拿大
     * 11：美国（除夏威夷）
     * 12：夏威夷
     * 13：加勒比海地区
     * 注：亚洲含中国大陆及港澳台地区，不含中东、西南太平洋
     */
    private static int[][] area=new int[14][14];

    /**
     * areaB矩阵存储始发地与目的地之间的航线的经济舱旅客可免费托运1or2件行李
     * area[i][j]中i代表始发地，j代表目的地，m代表航班属于区域m;矩阵为对称矩阵
     * area[i][j]值为true表示可免费托运2件行李，为false表示只可托运一件行李
     * 数字的地区表示与area矩阵相同
     */
    private static boolean[][] areaB=new boolean[14][14];

    /**
     * 求解国际航班的区域
     * @return 区域序号
     */
    public static int AreaCalculate(int x,int y){
        //地区矩阵初始化
        for(int i=0; i<14; i++){
            for(int j=0; j<14; j++){
                area[i][j]=5;
            }
        }
        //区域一
        for(int i=0; i<9; i++){
            area[i][13]=1;
            area[i][9]=1;
            //矩阵对称
            area[13][i]=1;
            area[9][i]=1;
        }
        //区域二
        for(int i=0; i<9; i++){
            if(i==0||i==1||i==2||i==3||i==4||i==5||i==8){
                area[i][6]=2;
                area[i][7]=2;
                //矩阵对称
                area[6][i]=2;
                area[7][i]=2;
            }
            if(i==5){
                area[i][1]=2;
                area[1][i]=2;
            }
            if(i==0||i==2||i==3||i==4||i==8){
                area[i][1]=2;
                area[i][5]=2;
                //矩阵对称
                area[1][i]=2;
                area[5][i]=2;
            }
        }
        //区域三
        for(int i=0; i<14; i++){
            if(i!=10&&i!=11&&i!=12){
                area[i][10]=3;
                //矩阵对称
                area[10][i]=3;
            }
        }
        //区域四
        for(int i=0; i<10; i++){
            if(i!=11&&i!=12){
                area[i][11]=4;
                area[i][12]=4;
                //矩阵对称
                area[11][i]=4;
                area[12][i]=4;
            }
        }
        //其它地区间的航线属于区域五

        return area[x][y];
    }

    /**
     * 求解国际航班经济舱旅客是否可免费托运两件行李
     * @return 该航班经济舱旅客是否可免费托运两件行李
     */
    public static boolean AreaBCalculate(int x,int y){
        for(int i=0; i<14; i++){
            if(i==9||i==10||i==11||i==13){
                for(int j=0; j<9; j++){
                    areaB[i][j]=true;
                    areaB[j][i]=true;
                }
            }
            if(i<9&&i!=6){
                areaB[i][6]=true;
                areaB[6][i]=true;
            }
            if(i<6&&i!=0){
                areaB[i][0]=true;
                areaB[i][7]=true;
                areaB[i][8]=true;
                //矩阵对称
                areaB[0][i]=true;
                areaB[7][i]=true;
                areaB[8][i]=true;
            }
        }

        return areaB[x][y];
    }
}
