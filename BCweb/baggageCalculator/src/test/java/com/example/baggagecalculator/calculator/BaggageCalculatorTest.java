package com.example.baggagecalculator.calculator;

import org.junit.Assert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Vector;

import static org.junit.jupiter.api.Assertions.*;

class BaggageCalculatorTest {

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void baggageCalculatorD() {
        Vector<Integer> carryB=new Vector<>();
        Vector<Integer> checkB=new Vector<>();
        Vector<Integer> extraB=new Vector<>();
        Vector<Integer> overB=new Vector<>();
        Vector<Integer> unableB=new Vector<>();
        Passenger passenger1=new Passenger(0,0,4,true);
        Passenger passenger2=new Passenger(3,0,0,true);
        Passenger passenger3=new Passenger(5,0,1,true);
        passenger1.setPrice(1000);
        passenger2.setPrice(1000);
        passenger3.setPrice(1000);
        Baggage baggage1=new Baggage(40,20,15,1,false);
        Baggage baggage2=new Baggage(40,20,15,5,true);
        Baggage baggage3=new Baggage(40,20,15,6,false);
        Baggage baggage4=new Baggage(15,20,15,7,false);
        Baggage baggage5=new Baggage(40,20,15,6,true);
        Baggage baggage6=new Baggage(40,20,15,15,false);
        Baggage baggage7=new Baggage(40,20,15,18,false);
        Baggage baggage8=new Baggage(40,20,15,28,false);
        Baggage baggage9=new Baggage(40,20,15,32,false);
        Baggage baggageA=new Baggage(40,20,15,38,false);

        Baggage[] B2={baggage2,baggage4,baggage5,baggage8,baggage9};
        Baggage[] B1={baggage2,baggage6,baggage7,baggage8,baggageA};
        Baggage[] B3={baggage1,baggage2,baggage3,baggage7,baggage8};
        assertEquals(BaggageCalculator.baggageCalculatorD(passenger1,B1,carryB,checkB,extraB,overB,unableB),225);
        carryB.clear();checkB.clear();extraB.clear();overB.clear();unableB.clear();
        assertEquals(BaggageCalculator.baggageCalculatorD(passenger2,B2,carryB,checkB,extraB,overB,unableB),420);
        carryB.clear();checkB.clear();extraB.clear();overB.clear();unableB.clear();
        assertEquals(BaggageCalculator.baggageCalculatorD(passenger3,B3,carryB,checkB,extraB,overB,unableB),90);
    }

    @Test
    void baggageCalculatorI() {
        Vector<Integer> carryB=new Vector<>();
        Vector<Integer> checkB=new Vector<>();
        Vector<Integer> extraB=new Vector<>();
        Vector<Integer> overB=new Vector<>();
        Vector<Integer> unableB=new Vector<>();
        Passenger passenger1=new Passenger(2,0,1,false);
        Passenger passenger2=new Passenger(4,0,3,false);
        Passenger passenger3=new Passenger(5,0,1,true);
        passenger1.setArea(1);
        passenger2.setArea(2);
        Baggage baggage1=new Baggage(40,20,15,4,false);
        Baggage baggage2=new Baggage(40,20,15,5,false);
        Baggage baggage3=new Baggage(40,20,15,7,false);
        Baggage baggage4=new Baggage(40,20,15,18,false);
        Baggage baggage5=new Baggage(40,20,15,20,false);
        Baggage baggage6=new Baggage(40,20,15,27,false);
        Baggage baggage7=new Baggage(40,20,15,10,true);
        Baggage baggage8=new Baggage(80,60,40,29,false);
        Baggage[] B1={baggage1,baggage2,baggage4,baggage5,baggage6};
        Baggage[] B2={baggage1,baggage2,baggage3,baggage6,baggage7,baggage8};
        int[] result1={3780,590};
        Assert.assertArrayEquals(BaggageCalculator.baggageCalculatorI(passenger1,B1,carryB,checkB,overB,unableB),result1);
        carryB.clear();checkB.clear();overB.clear();unableB.clear();
        int[] result2={2200,320,280};
        Assert.assertArrayEquals(BaggageCalculator.baggageCalculatorI(passenger2,B2,carryB,checkB,overB,unableB),result2);
    }
}