package com.example.baggagecalculator.calculator;

import org.junit.Assert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class GetWeightAndNumTest {

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void maxWeightAndNum() {
        Passenger passenger1= new Passenger(0,2,0,true);
        int[] result1={1,10};
        Assert.assertArrayEquals(GetWeightAndNum.MaxWeightAndNum(passenger1),result1);
        Passenger passenger2= new Passenger(0,0,1,true);
        int[] result2={1,20};
        Assert.assertArrayEquals(GetWeightAndNum.MaxWeightAndNum(passenger2),result2);
        Passenger passenger3= new Passenger(3,1,0,true);
        int[] result3={7,40};
        Assert.assertArrayEquals(GetWeightAndNum.MaxWeightAndNum(passenger3),result3);
        Passenger passenger4= new Passenger(5,0,4,true);
        int[] result4={1,30};
        Assert.assertArrayEquals(GetWeightAndNum.MaxWeightAndNum(passenger4),result4);
        Passenger passenger5= new Passenger(2,0,2,false);
        int[] result5={2,23};
        Assert.assertArrayEquals(GetWeightAndNum.MaxWeightAndNum(passenger5),result5);
        Passenger passenger6= new Passenger(4,1,3,false);
        int[] result6={2,32};
        Assert.assertArrayEquals(GetWeightAndNum.MaxWeightAndNum(passenger6),result6);
        Passenger passenger7= new Passenger(0,0,0,false);
        passenger7.setAreaB(true);
        int[] result7={2,23};
        Assert.assertArrayEquals(GetWeightAndNum.MaxWeightAndNum(passenger7),result7);
        Passenger passenger8= new Passenger(0,0,0,false);
        passenger7.setAreaB(false);
        int[] result8={1,23};
        Assert.assertArrayEquals(GetWeightAndNum.MaxWeightAndNum(passenger8),result8);
    }

    @Test
    void extraWeight() {
        Passenger passenger1=new Passenger(0,0,0,true);
        Assert.assertEquals(GetWeightAndNum.ExtraWeight(passenger1),0);
        Passenger passenger2=new Passenger(1,1,2,true);
        Assert.assertEquals(GetWeightAndNum.ExtraWeight(passenger2),20);
        Passenger passenger3=new Passenger(3,0,5,true);
        Assert.assertEquals(GetWeightAndNum.ExtraWeight(passenger3),30);
        Passenger passenger4=new Passenger(2,0,3,false);
        Assert.assertEquals(GetWeightAndNum.ExtraWeight(passenger4),0);
    }

    @Test
    void carryNum() {
        Passenger passenger1=new Passenger(0,0,2,true);
        Assert.assertEquals(GetWeightAndNum.CarryNum(passenger1),1);
        Passenger passenger2=new Passenger(4,0,4,false);
        Assert.assertEquals(GetWeightAndNum.CarryNum(passenger2),2);
    }
}