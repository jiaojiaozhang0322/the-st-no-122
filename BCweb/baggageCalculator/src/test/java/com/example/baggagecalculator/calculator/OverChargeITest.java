package com.example.baggagecalculator.calculator;

import org.junit.Assert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class OverChargeITest {

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void overWSChargeI() {
        Passenger passenger=new Passenger(1,0,1,false);
        Baggage baggage1=new Baggage(20,20,18,20,false);
        Baggage baggage2=new Baggage(40,30,20,27,false);
        Baggage baggage3=new Baggage(50,20,18,30,false);
        Baggage baggage4=new Baggage(60,60,50,21,false);
        Baggage baggage5=new Baggage(70,50,50,32,false);
        int[] result0={0,0},result1={0,0,0};

        passenger.setArea(1);
        Assert.assertArrayEquals(OverChargeI.OverWSChargeI(passenger,baggage1),result0);
        int[] result2={380,60};
        Assert.assertArrayEquals(OverChargeI.OverWSChargeI(passenger,baggage2),result2);
        result2[0]=980;result2[1]=150;
        Assert.assertArrayEquals(OverChargeI.OverWSChargeI(passenger,baggage3),result2);
        Assert.assertArrayEquals(OverChargeI.OverWSChargeI(passenger,baggage4),result2);
        result2[0]=1400;result2[1]=220;
        Assert.assertArrayEquals(OverChargeI.OverWSChargeI(passenger,baggage5),result2);
        //继续对区域2~5进行测试
        //......

        passenger.setArea(2);
        Assert.assertArrayEquals(OverChargeI.OverWSChargeI(passenger,baggage1),result1);
        int[] result3={280,40,35};
        Assert.assertArrayEquals(OverChargeI.OverWSChargeI(passenger,baggage2),result3);
        result3[0]=690;result3[1]=100;result3[2]=85;
        Assert.assertArrayEquals(OverChargeI.OverWSChargeI(passenger,baggage3),result3);
        Assert.assertArrayEquals(OverChargeI.OverWSChargeI(passenger,baggage4),result3);
        result3[0]=1100;result3[1]=160;result3[2]=140;
        Assert.assertArrayEquals(OverChargeI.OverWSChargeI(passenger,baggage5),result3);

        passenger.setArea(3);
        Assert.assertArrayEquals(OverChargeI.OverWSChargeI(passenger,baggage1),result1);
        result3[0]=520;result3[1]=75;result3[2]=100;
        Assert.assertArrayEquals(OverChargeI.OverWSChargeI(passenger,baggage2),result3);
        result3[0]=520;result3[1]=75;result3[2]=100;
        Assert.assertArrayEquals(OverChargeI.OverWSChargeI(passenger,baggage2),result3);
        Assert.assertArrayEquals(OverChargeI.OverWSChargeI(passenger,baggage3),result3);
        Assert.assertArrayEquals(OverChargeI.OverWSChargeI(passenger,baggage4),result3);
        Assert.assertArrayEquals(OverChargeI.OverWSChargeI(passenger,baggage5),result3);

        passenger.setArea(4);
        Assert.assertArrayEquals(OverChargeI.OverWSChargeI(passenger,baggage1),result0);
        result2[0]=690;result2[1]=100;
        Assert.assertArrayEquals(OverChargeI.OverWSChargeI(passenger,baggage2),result2);
        result2[0]=1040;result2[1]=150;
        Assert.assertArrayEquals(OverChargeI.OverWSChargeI(passenger,baggage3),result2);
        Assert.assertArrayEquals(OverChargeI.OverWSChargeI(passenger,baggage4),result2);
        result2[0]=2050;result2[1]=300;
        Assert.assertArrayEquals(OverChargeI.OverWSChargeI(passenger,baggage5),result2);

        passenger.setArea(5);
        Assert.assertArrayEquals(OverChargeI.OverWSChargeI(passenger,baggage1),result0);
        result2[0]=210;result2[1]=30;
        Assert.assertArrayEquals(OverChargeI.OverWSChargeI(passenger,baggage2),result2);
        result2[0]=520;result2[1]=75;
        Assert.assertArrayEquals(OverChargeI.OverWSChargeI(passenger,baggage3),result2);
        Assert.assertArrayEquals(OverChargeI.OverWSChargeI(passenger,baggage4),result2);
        result2[0]=830;result2[1]=120;
        Assert.assertArrayEquals(OverChargeI.OverWSChargeI(passenger,baggage5),result2);
    }

    @Test
    void overNumChargeI() {
        Passenger passenger=new Passenger(1,0,1,false);
        int[] result0={0,0},result1={0,0,0};

        passenger.setArea(1);
        Assert.assertArrayEquals(OverChargeI.OverNumChargeI(passenger,0),result0);
        int[] result2={1400,220};
        Assert.assertArrayEquals(OverChargeI.OverNumChargeI(passenger,1),result2);
        result2[0]=3400;result2[1]=530;
        Assert.assertArrayEquals(OverChargeI.OverNumChargeI(passenger,2),result2);
        result2[0]=6400;result2[1]=990;
        Assert.assertArrayEquals(OverChargeI.OverNumChargeI(passenger,3),result2);
        //继续对区域2~5进行测试
        //......

        passenger.setArea(2);
        Assert.assertArrayEquals(OverChargeI.OverNumChargeI(passenger,0),result1);
        int[] result3={1100,160,140};
        Assert.assertArrayEquals(OverChargeI.OverNumChargeI(passenger,1),result3);
        result3[0]=3790;result3[1]=550;result3[2]=480;
        Assert.assertArrayEquals(OverChargeI.OverNumChargeI(passenger,3),result3);

        passenger.setArea(3);
        Assert.assertArrayEquals(OverChargeI.OverNumChargeI(passenger,0),result1);
        result3[0]=1170;result3[1]=110;result3[2]=225;
        Assert.assertArrayEquals(OverChargeI.OverNumChargeI(passenger,1),result3);
        result3[0]=3930;result3[1]=450;result3[2]=750;
        Assert.assertArrayEquals(OverChargeI.OverNumChargeI(passenger,3),result3);

        passenger.setArea(4);
        Assert.assertArrayEquals(OverChargeI.OverNumChargeI(passenger,0),result0);
        result2[0]=2760;result2[1]=400;
        Assert.assertArrayEquals(OverChargeI.OverNumChargeI(passenger,2),result2);
        result2[0]=4350;result2[1]=630;
        Assert.assertArrayEquals(OverChargeI.OverNumChargeI(passenger,3),result2);

        passenger.setArea(5);
        Assert.assertArrayEquals(OverChargeI.OverNumChargeI(passenger,0),result0);
        result2[0]=830;result2[1]=120;
        Assert.assertArrayEquals(OverChargeI.OverNumChargeI(passenger,1),result2);
        result2[0]=1930;result2[1]=280;
        Assert.assertArrayEquals(OverChargeI.OverNumChargeI(passenger,2),result2);
        result2[0]=3520;result2[1]=510;
        Assert.assertArrayEquals(OverChargeI.OverNumChargeI(passenger,3),result2);
    }
}