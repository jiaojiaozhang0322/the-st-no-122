package com.example.baggagecalculator.calculator;

import org.junit.Assert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AreaCalculatorTest {

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void areaCalculate() {
        Assert.assertEquals(AreaCalculator.AreaCalculate(7,9),1);
        Assert.assertEquals(AreaCalculator.AreaCalculate(6,2),2);
        Assert.assertEquals(AreaCalculator.AreaCalculate(7,10),3);
        Assert.assertEquals(AreaCalculator.AreaCalculate(12,9),4);
        Assert.assertEquals(AreaCalculator.AreaCalculate(11,13),5);
    }

    @Test
    void areaBCalculate() {
        Assert.assertEquals(AreaCalculator.AreaBCalculate(3,6),true);
        Assert.assertEquals(AreaCalculator.AreaBCalculate(7,8),false);
    }
}