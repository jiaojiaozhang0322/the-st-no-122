import { createRouter, createWebHistory } from 'vue-router'
import FirstView from '../views/FirstView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: FirstView
    },
    {
      path: '/PD',
      name: 'PD',
      component: () => import('../views/PDview.vue')
    },
    {
      path: '/PI',
      name: 'PI',
      component: () => import('../views/PIview.vue')
    },
    {
      path: '/result',
      name: 'result',
      component: () => import('../views/ResultView.vue')
    },
    {
      path: '/bg',
      name: 'bg',
      component: () => import('../components/icons/Baggage.vue')
    },
    {
      path: '/test',
      name: 'test',
      component: () => import('../views/TestView.vue')
    },
  ]
})

export default router
